### Keybase proof

I hereby claim:

  * I am puckipedia on github.
  * I am puckipedia (https://keybase.io/puckipedia) on keybase.
  * I have a public key whose fingerprint is 462B B528 E366 5F37 3F26  3303 5732 E243 55BA F33E

To claim this, I am signing this object:

```json
{
    "body": {
        "client": {
            "name": "keybase.io node.js client",
            "version": "0.8.25"
        },
        "key": {
            "eldest_kid": "010144216cfc072daf6f62e025d007279b20331034278bfc0be2e6d2d379e8bed1c40a",
            "fingerprint": "462bb528e3665f373f2633035732e24355baf33e",
            "host": "keybase.io",
            "key_id": "5732e24355baf33e",
            "kid": "010144216cfc072daf6f62e025d007279b20331034278bfc0be2e6d2d379e8bed1c40a",
            "uid": "c497eb586d68eb31da382a66c2cc8b19",
            "username": "puckipedia"
        },
        "service": {
            "name": "github",
            "username": "puckipedia"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1449333952,
    "expire_in": 157680000,
    "prev": "e11576ea591dc464aab284a171a49ee905ab9e244c299b5d9570d8994906898c",
    "seqno": 3,
    "tag": "signature"
}
```

with the key [462B B528 E366 5F37 3F26  3303 5732 E243 55BA F33E](https://keybase.io/puckipedia), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Version: GnuPG v2

owGtUn1MVWUc5kMQMKDkFrg5wDPlDrtezjnv+bx+ML7s3onIHFku43Y+3nM5crv3
dr+IkGhYoUOwnIOuoE4+xpzlMBVyIAFDxiXZSMuEpGWIwqghM0O0YecwWrW1/ur9
631/7/M87/N7fu9HkcEBYYH1W35MMO3YdCnwqyk+YJeg6S5BeLtYjBhKEMEqQ5tb
3dm4NyFiQAphMc+5oF62J9rsItTvdSUuYXSIFzpdst2moFA9o8dJpFSn4lU2tIrQ
5TYXyqJ6i6EYQeAYJUgCSuMiJ1EShUMUJ0VUOdMsj6MAYCggcJrhFQwPcUiJuAho
FjI8FDGBQDnlQUm2WaDT4ZRViwhB4TxP4gwEFEVKgAYSTgGAApIGOMQJQJI8JwEA
FWKB3eX+RzPIolPzor1/wf/Pvj2LcgLB0pAnGUqkGMgDTOQAg3MUJeCCwPAYqwJd
0LkUvMMjFMoOKMqcGqtS98oC/NtgLLK7wMP/F8dd7FCLRZA3L9HNvGwTlQz/mt1R
xIApUMEtqwJKtywAgCVxHQLfdshOaJaV8WIkTTGosnSIwwm9iibE1BrkSBYTBYIi
OI7HGYLDaIwjWAhZlOR4VsmUEHCW5UmRJWlUZFiWYFGKYRkBUTt6y2ZHDEDxyVkU
SZdssXFujxMipQeDVi8LCAwLCA0JUn9nQET4s39+2f6R6KeesaCq8z0xN3OcRxN+
2/dJckbzXHb6g/aqQ08+8Cc9evXmqS/msn8YipHwO91XGzdM7XsvLw0pmz69d5dR
O+2hr6x6N3a+U+hDMGP/hW+rwKmkqZkT29pm8k9cSMlFRnoqX5kkfrrWd3bieJq/
qSZ5d//+KO+Qr672YVpkg6kx+rMpf03uQNvV+OAV+aNMyhWDdH2z1VjHGfdUG7M6
rPufX4iLvz9KTiQJdb8OXk7xHDxnb1qfsDB2Y2xWY4xbdrJ194HQumcuPRl+uash
tV0+mbTWVz56JueW3ZedfidqR9BAWN51V+P7zc6N9Ruw4NmV5zeOIrJBqgRUpndb
UYF/9ez02Z1vtBpDfN+Y+iJKtPkLqeUJuXp/XNea6KHkeVdvbO9OoAk9ktWje7wu
8/vXG7TdW31YR3tL2mDw4YoI7wu17XnjFYNSveuXA7F94abOzc8Ruomk7mNlxsli
adj6e7Pm1opzmobay+CxefxBeZteMuzRvpafibivfT1uDa8JvDd9sW3r4Yje+JdG
5panx5WOrufzttye/xm9bbOMmyrd95fP+SKxjydbqv29E0gLsc7TeaZp8lOirKK5
Q95effreQuKjtcGp1UUzlpDYEWet5mn4GlAmbSoyfxk5cHFY+3nr3Yc34lb6Yqoy
gPGdrOP8ixmrvvuwJ+puV210jqRNO/IH
=dnox
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/puckipedia

### From the command line:

Consider the [keybase command line program](https://keybase.io/docs/command_line).

```bash
# look me up
keybase id puckipedia

# encrypt a message to me
keybase encrypt puckipedia -m 'a secret message...'

# ...and more...
```